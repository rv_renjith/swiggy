import React, { Component } from "react";
import "./styles.css";

class ListItem extends Component {
  constructor(props) {
    super(props);
    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(event) {
    this.props.updateCart(event);
  }

  render() {
    return <div className="List-Wrapper">
      <div className='List-Container' onClick={() => this._handleChange(this.props.details)}>
        <img src={this.props.details.img_url} alt={this.props.details.name} className='List-Image'/>
        <span className="List-Name">
          <img src={this.props.details.type_icon} alt={this.props.details.name} className='List-Type-Image'/>
          {this.props.details.name}
          <span className='List-Qty'>({this.props.details.qty_left})</span>
        </span>
        <span className='List-Eta'>ETA: {this.props.details.expected_eta}</span>
      </div>
    </div>;
  }
}

export default ListItem;
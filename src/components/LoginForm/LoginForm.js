import React, { Component } from "react";
import "./styles.css";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null
    }
    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  _validateForm() {
    if (
      !this.state.username ||
      this.state.username.length < 4 ||
      !this.state.password
    ) {
      this.setState({
        errorMessage: "Field Validation error"
      });
      return false;
    } else {
      window.location = "/home";
    }
  }

  render() {
    return <div className="Wrapper">
      <div className="Form-Container">
        <h3 className="Form-title">Swiggy Breakfast Login</h3>
        <input type='number' placeholder='Cell Number' className="Swiggy-input" name='username' onChange={this._handleChange}/>
        <input type='password' placeholder='Password' className="Swiggy-input" name='password' onChange={this._handleChange}/>
        <button className="Swiggy-button" onClick={() => this._validateForm()}>Login</button>
        {this.state.errorMessage && <span className='Error-message'>{this.state.errorMessage}</span>}
      </div>
    </div>;
  }
}

export default LoginForm;
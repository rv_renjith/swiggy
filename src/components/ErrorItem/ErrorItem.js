import React, { Component } from "react";
import "./styles.css";

class ErrorItem extends Component {
  
  render() {
    return <div className="Wrapper">
    <div className="Form-Container">
      <h3 className="Form-title">{this.props.error}</h3>
    </div>
  </div>;
  }
}

export default ErrorItem;
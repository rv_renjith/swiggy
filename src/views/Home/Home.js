import React, { Component } from "react";
import "./styles.css";
import ListItem from '../../components/ListItem';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCart: [],
      breakfast : [
        {
          'name': 'Idli',
          'img_url': 'https://www.indianhealthyrecipes.com/wp-content/uploads/2013/01/soft-idli-recipe-with-idli-rava.jpg',
          'id': '1',
          'qty_left': '4',
          'expected_eta': '15 minutes',
          'type_icon': 'https://www.pngkey.com/png/detail/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png'
        },
        {
          'name': 'Dosa',
          'img_url': 'https://static01.nyt.com/images/2015/01/28/dining/28KITCHEN1/28KITCHEN1-superJumbo.jpg',
          'id': '2',
          'qty_left': '4',
          'expected_eta': '25 minutes',
          'is_veg': true,
          'type_icon': 'https://www.pngkey.com/png/detail/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png'
        },
        {
          'name': 'Upma',
          'img_url': 'https://www.vegrecipesofindia.com/wp-content/uploads/2018/09/upma-recipe-3-500x375.jpg',
          'id': '3',
          'qty_left': '4',
          'expected_eta': '45 minutes',
          'is_veg': true,
          'type_icon': 'https://www.pngkey.com/png/detail/261-2619381_chitr-veg-symbol-svg-veg-and-non-veg.png'
        },
      ]
    }
    this._updateCart = this._updateCart.bind(this);
  }

  _updateCart = (item) => {
    var result = this.state.selectedCart.filter((cart) => cart.id === item.id);
    console.log('result', result)
    if (result && result.length > 0) {
      var stateValue = this.state.selectedCart;
      var spliceIndex = stateValue.indexOf(item);
      var splicedArray = stateValue.splice(spliceIndex, 1);
      this.setState((prevState, prevProps) => {
        return {
          selectedCart: [splicedArray]
        };
      });
    } else {
      this.setState((prevState, prevProps) => {
        return {selectedCart: [
          ...this.state.selectedCart,
          item
        ]};
      });
    }
    console.log('updateCart', this.state.selectedCart)
  }

  _placeOrder = () => {
    window.location = "/success";
  }

  render() {
    return <div className="Wrapper">
      <div className="Form-Container">
        <h3 className="Form-title">Pick your breakfast</h3>
        <div className="Form-Wrap">
          {this.state.breakfast.map((item, index) => {
            return (
              <ListItem key={index} details={item} updateCart={() => this._updateCart(item)}/>
            );
          })}
        </div>
        {this.state.selectedCart.length > 0 && <button className="Swiggy-confirm" onClick={() => this._placeOrder()}>PLACE ORDER</button>}
      </div>
    </div>;
  }
}

export default Home;
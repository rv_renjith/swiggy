import React, { Component } from "react";
import "./styles.css";

class Success extends Component {

  _exitOrder = () => {
    window.location = "/home";
  }

  render() {
    return <div className="Wrapper">
      <div className="Form-Container">
        <h3 className="Form-title">Your order has been placed.</h3>
        <button className="Swiggy-confirm" onClick={() => this._exitOrder()}>EXIT</button>
      </div>
      
    </div>;
  }
}

export default Success;
import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { UserAgent } from "react-useragent";
import Home from "../views/Home";
import Login from "../views/Login";
import Success from "../views/Success";
import ErrorItem from "../components/ErrorItem";


class Root extends Component {
  render() {
    return (

        <BrowserRouter>
          <UserAgent render={({ ua }) => {
            return ua.mobile ? 
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/home" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/success" component={Success} />
              </Switch>
          : <ErrorItem error={'Feature coming soon on desktop'}/>;
          }} />
        </BrowserRouter>
    );
  }
}

export default Root;